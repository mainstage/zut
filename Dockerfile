FROM debian:stable

ARG uid 1000

# Install all packages
COPY scripts/install_packages.sh /tmp/scripts/
RUN /tmp/scripts/install_packages.sh

# Setting up entrypoint
COPY scripts/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Setting up user
COPY scripts/setup_user.sh /tmp/scripts/
RUN /tmp/scripts/setup_user.sh $uid

USER zut

COPY scripts/wait.sh /wait.sh
CMD ["/wait.sh"]
