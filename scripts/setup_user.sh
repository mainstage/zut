#! /bin/bash

set -o errexit
set -o nounset
set -o pipefail
if [ "${TRACE-0}" == "1" ]; then set -o xtrace; fi

uid=${1:-1000}
# if [ "$uid" = "" ]; then
#     echo "$0 uid"
# fi

echo "Creating user ($uid)"
adduser --uid "$uid" --disabled-password --gecos="Zut,,,," zut

echo "zut ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/zut
