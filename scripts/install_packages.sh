#! /bin/bash

set -o errexit
set -o nounset
set -o pipefail
if [ "${TRACE-0}" == "1" ]; then set -o xtrace; fi


apt-get update && apt-get upgrade -y
apt-get install --no-install-recommends -qy \
        ca-certificates \
        curl \
        emacs-nox \
        file \
        git-core \
        iftop \
        jq \
        libreadline8 \
        lsof \
        nano \
        openssl \
        rclone \
        s3cmd \
        socat \
        strace \
        sudo \
        tree \
        vim \
        wget \
        zsh

rm -rf /var/lib/apt/lists

echo "Fetching minio client 'mc':"
curl -s https://dl.min.io/client/mc/release/linux-amd64/mc -o /usr/local/bin/mc
chmod +x /usr/local/bin/mc
