# Zut !

> "Zut !" is a quite polite way to say "Shit !" or "Fuck !" in french

Zut is swiss army knife container to troubleshoot problems with your application
or services in Kubernetes. It contains a variety of useful tools to have waiting
for you in your cluster.

## Included tools

- Shells: bash, zsh
- Editors: emacs, vim, nano
- http clients: curl, wget
- jq
- openssl
- Storage clients: s3cmd, rclone, mc (minio-client)

You can always add more from inside the image using:

``` shellsession
$> sudo apt-get update
[...]
$> sudo apt-get install my-package
```

## Usage in kubernetes

This image is intended to be a long-running image in your k8s cluster,
to have a nice place from where to troubleshoot any issue you encounter.

The image has been intended to support having a PVC mounted in `/home/zut`
to store your configuration and shell history.

For your convenience, a helm chart to deploy a single pod with a PVC is
available:

### Helm chart

You can deploy zut in your cluster with helm. Feel free to look at the
`values.yaml` for the few configurations options it provides.

Add the zut helm repository:

`helm repo add zut https://gitlab.com/api/v4/projects/40832031/packages/helm/stable`

Deploy zut! :

`helm install zut zut/zut --set home.persistence.size=512Mi`

## Contributing.

Feel free to fork and send merge requests with new tools and configuration bits

## License

This repository is licensed under the MIT License, see `LICENSE` file.

## Thanks

This image was inspired by netshoot: https://github.com/nicolaka/netshoot
